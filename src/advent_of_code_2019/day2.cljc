(ns advent-of-code-2019.day2
  (:require [ysera.test :refer [is= is is-not]]))

(def puzzle-input [1 12 2 3 1 1 2 3 1 3 4 3 1 5 0 3 2 6 1 19 1 5 19 23 2 6 23 27 1 27 5 31 2 9 31 35 1 5 35 39 2 6 39 43 2 6 43 47 1 5 47 51 2 9 51 55 1 5 55 59 1 10 59 63 1 63 6 67 1 9 67 71 1 71 6 75 1 75 13 79 2 79 13 83 2 9 83 87 1 87 5 91 1 9 91 95 2 10 95 99 1 5 99 103 1 103 9 107 1 13 107 111 2 111 10 115 1 115 5 119 2 13 119 123 1 9 123 127 1 5 127 131 2 131 6 135 1 135 5 139 1 139 6 143 1 143 6 147 1 2 147 151 1 151 5 0 99 2 14 0 0])

(defn day-1-a
  {:test (fn []
           (is= (day-1-a [1 0 0 0 99]) 2))}
  [puzzle-input]
  (-> (loop [position 0
             result puzzle-input]
        (if (or (= position 0) (contains? result (+ 5 position)))
          (let [operation-decider (nth result position)
                first-value-index (nth result (+ position 1))
                second-value-index (nth result (+ position 2))
                position-to-change (nth result (+ position 3))]
            (condp = operation-decider
              1 (recur (+ 4 position) (assoc result position-to-change (+ (nth result first-value-index) (nth result second-value-index))))
              2 (recur (+ 4 position) (assoc result position-to-change (* (nth result first-value-index) (nth result second-value-index))))
              99 result
              (recur (+ 4 position) result)))
          result))
      (first)))


(day-1-a puzzle-input)

(defn day-2-b
  [puzzle-input]
  (let [noun-verb (->> (map (fn [noun]
                              (map (fn [verb] [noun verb]) (range 0 100)))
                            (range 0 100))
                       (flatten)
                       (partition 2)
                       (reduce (fn [result value]
                                 (println "val" value "result" result)
                                 (if (= (:result result) 19690720)
                                   (reduced (:value result))
                                   {:result (day-1-a (-> puzzle-input
                                                 (assoc 1 (first value))
                                                 (assoc 2 (second value))))
                                    :value value}))
                               nil))]
    (+ (* 100 (first noun-verb)) (second noun-verb))))

(day-2-b puzzle-input)


