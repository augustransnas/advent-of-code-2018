(ns advent-of-code-2018.day8
  (:require [ysera.test :refer [is= is is-not]]
            [clojure.string :as str]))

(def test-input [2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2])

(defn should-create-child-node?
  [node]
  (and (> (first (:header node)) 0)
       (= (count (:child-nodes node)) (first (:header node)))))

(defn should-update-header?
  {:test (fn []
           (is= (should-update-header? {}) true)
           (is= (should-update-header? {:header [0]}) true)
           (is= (should-update-header? {:header [0 1]}) false))}
  [node]
  (< (count (:header node)) 2))

(defn should-create-new-node?
  {:test (fn []
           (is= (should-create-new-node? []) true)
           (is= (should-create-new-node? {:header [2]}) false)
           (is= (should-create-new-node? {:header      [2 3]
                                          :child-nodes [{:header [0 3]}]}) false)
           (is= (should-create-new-node? {:header      [2 3]
                                          :child-nodes [{:header    [0 3]
                                                         :meta-data [10 11 12]}
                                                        {:header      [1 1]
                                                         :child-nodes [{:header    [0 1]
                                                                        :meta-data [99]}]
                                                         :meta-data   [2]}]}) false)
           (is= (should-create-new-node? {:header      [2 3]
                                          :child-nodes [{:header    [0 3]
                                                         :meta-data [10 11 12]}
                                                        {:header      [1 1]
                                                         :child-nodes [{:header    [0 1]
                                                                        :meta-data [99]}]
                                                         :meta-data   [2]}]
                                          :meta-data   [1 1 2]}) true)
           )}
  [node]
  (or (empty? (:header node))
      (and (= (count (:header node)) 2)
           (= (count (:child-nodes node)) (first (:header node)))
           (= (count (:meta-data node)) (last (:header node))))))



(defn day8-a
  {:test (fn [] (is= (day8-a test-input) [{:header      [2 3]
                                           :child-nodes [{:header    [0 3]
                                                          :meta-data [10 11 12]}
                                                         {:header      [1 1]
                                                          :child-nodes [{:header    [0 1]
                                                                         :meta-data [99]}]
                                                          :meta-data   [2]}]
                                           :meta-data   [1 1 2]}]))}
  [input]
  (reduce (fn [nodes num]
            (let [current-node (last nodes)]
              (cond (should-create-new-node? current-node)
                    (conj nodes {:header [num]})
                    (should-update-header? current-node)
                    (conj (butlast nodes) (update current-node :header (fn [current-header]
                                                                         (conj current-header num))))
                    (should-create-child-node? current-node)
                    nodes)

              )) [] input))