(ns advent-of-code-2019.day1
  (:require [ysera.test :refer [is= is is-not]]))

(def puzzle-input [88519 117448 108285 121718 64281 56719 69305 116255 85453 137199 122492 70729 149169 86016 74378 135644 141249 63641 61727 116510 60001 85691 66437 112556 73871 77359 110859 124192 120229 64408 139830 148029 65862 84766 146598 106148 76313 132619 126445 124908 136279 86875 87832 74624 139808 93470 129969 55892 122861 58742 116963 111110 145501 112617 71513 104804 83016 136218 50194 146859 148831 140601 92783 85701 124258 80228 100029 120209 95788 86545 148580 129744 117411 90408 104237 100642 98879 145146 121711 98084 129186 69759 92088 146954 143940 60302 110277 110550 134676 99848 120876 97516 134259 68168 69821 54549 134847 72155 143606 74241])

(defn calculate-fuel
  {:test (fn []
           (is= (calculate-fuel 12) 2)
           (is= (calculate-fuel 14) 2)
           (is= (calculate-fuel 1969) 654)
           (is= (calculate-fuel 100756) 33583))}
  [mass]
  (-> mass
      (quot 3)
      (- 2)))

(defn day-1-a
  {:test (fn []
           (is= (day-1-a [12 14 1969 100756]) 34241))}
  [puzzle-input]
  (reduce (fn [result mass]
            (+ result (calculate-fuel mass)))
          0
          puzzle-input))

(day-1-a puzzle-input)

(defn day-1-b
  {:test (fn []
           (is= (day-1-b [14 1969 100756]) 51314))}
  [puzzle-input]
  (reduce (fn [result mass]
            (let [total-fuel-required (loop [previous-mass mass
                                             total-fuel 0]
                                        (let [fuel (calculate-fuel previous-mass)]
                                          (if (<= fuel 0)
                                            total-fuel
                                            (recur fuel (+ total-fuel fuel)))))]
              (+ result total-fuel-required)))
          0
          puzzle-input))

(day-1-b puzzle-input)







